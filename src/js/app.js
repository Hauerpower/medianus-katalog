import generalInit from './modules/general';
import slidersInit from './modules/sliders';
import menuInit from './modules/menu';
import galleryInit from './modules/wp-gallery';
import loadMoreInit from './modules/loadmore';

(function($){
	
	$(document).foundation();
		
	$(document).ready(function(){

		generalInit();
		slidersInit();
		menuInit();
		galleryInit();
		loadMoreInit(); 

	});
	
})(jQuery);